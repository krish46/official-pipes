import os
import tempfile
import unittest

from build import BitbucketCloudService, ProcessMetadata, ProcessResult
import build
from unittest.mock import patch, MagicMock
import yaml

README = """# Bitbucket Pipelines Pipe: Trigger Bitbucket Pipelines build

Trigger Bitbucket Pipelines build

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/bitbucket-trigger-pipeline:1.0.0
  variables:
    APP_PASSWORD: $APP_PASSWORD
    REPO: 'your-awesome-repo'
    # ACCOUNT: '<string>' # Optional
    # REF_TYPE: '<string>' # Optional
```
## Variables

```yaml
  - pipe: atlassian/bitbucket-trigger-pipeline:1.0.0
    variables:
      APP_PASSWORD: $APP_PASSWORD
      REPO: 'your-awesome-repo'
```
"""

YAML_FRAGMENT = """- pipe: atlassian/bitbucket-trigger-pipeline:1.0.0
  variables:
    APP_PASSWORD: $APP_PASSWORD
    REPO: 'your-awesome-repo'
    # ACCOUNT: '<string>' # Optional
    # REF_TYPE: '<string>' # Optional
"""


def mock_successful_response(json=None, text=None):
    response = MagicMock()
    response.status_code = 200
    response.text = text
    if json:
        response.json = MagicMock(return_value=json)
    return response


class BitbucketCloudServiceTest(unittest.TestCase):

    @patch('build.requests.get')
    def test_extract_yml_definition_is_correct(self, mock_get):
        mock_get.return_value = mock_successful_response(text=README)
        service = BitbucketCloudService()
        self.assertEqual(YAML_FRAGMENT, service.get_yml_definition(YAML_FRAGMENT, '1.0.0'))

    @patch('build.requests.get')
    def test_extract_yml_definition_fails(self, mock_get):
        mock_get.return_value = mock_successful_response(text="""### YAML Definition""")
        service = BitbucketCloudService()
        self.assertEqual(None, service.get_yml_definition("""### YAML Definition""", '1.0.0'))

    @patch('build.requests.get')
    def test_latest_tag(self, mock_get):
        service = BitbucketCloudService()
        mock_get.return_value = mock_successful_response(json={'pagelen': 100, 'values': [{'name': '0.3.1'}, {'name': '0.3.0'}]})

        tag = service.get_latest_tag('atlassian/aws-cloudformation-deploy')
        self.assertEqual('0.3.1', tag)

    @patch('build.requests.get')
    def test_latest_tag_order(self, mock_get):
        service = BitbucketCloudService()
        mock_get.return_value = mock_successful_response(json={'pagelen': 100, 'values': [{'name': '0.10.1'}, {'name': '0.1.0'}]})

        tag = service.get_latest_tag('atlassian/aws-cloudformation-deploy')
        self.assertEqual('0.10.1', tag)


class PrepareResultsTest(unittest.TestCase):

    def test_extract_data(self):
        results = [
            ProcessResult(
                ProcessMetadata(file='pipes/a.yml', valid=True, failures=[], errors=[], skipped=[]),
                data={
                    'name': 'SSH run',
                    'description': 'Run a command or a script on your server',
                    'category': 'Utilities',
                    'repositoryPath': 'atlassian/ssh-run',
                    'version': '0.2.5',
                    'maintainer': {'name': 'Atlassian', 'website': 'https://www.atlassian.com/'},
                    'logo': 'https://bytebucket.org/ravatar/%7B7c34b996-416c-4937-a46a-701da15c1cfe%7D?ts=2162382',
                    'yml': "YAML file"
                }
            ),
            ProcessResult(
                ProcessMetadata(file='pipes/b.yml', valid=False, failures=['failure'], errors=[], skipped=[]),
                data={}
            )
        ]
        output = [
            {
                'name': 'SSH run',
                'description': 'Run a command or a script on your server',
                'category': 'Utilities',
                'repositoryPath': 'atlassian/ssh-run',
                'version': '0.2.5',
                'maintainer': {'name': 'Atlassian', 'website': 'https://www.atlassian.com/'},
                'logo': 'https://bytebucket.org/ravatar/%7B7c34b996-416c-4937-a46a-701da15c1cfe%7D?ts=2162382',
                'yml': "YAML file"
            }
        ]

        self.assertEqual(output, build.extract_data(results))


def configure_mock_bitbucket(mock_bitbucket_obj: MagicMock, repository_info, latest_tag, yaml_fragment, readme_exists, timestamp, pipe_yml):
    mock_bitbucket_obj.return_value.get_repository_info.return_value = repository_info
    mock_bitbucket_obj.return_value.get_latest_tag.return_value = latest_tag
    mock_bitbucket_obj.return_value.readme_exists.return_value = readme_exists
    mock_bitbucket_obj.return_value.get_yml_definition.return_value = yaml_fragment
    mock_bitbucket_obj.return_value.get_pipe_created_timestamp.return_value = timestamp
    mock_bitbucket_obj.return_value.get_pipe_metadata.return_value = yaml.safe_load(pipe_yml)


def configure_mock_docker(mock_docker_obj: MagicMock, size: int) -> None:
    mock_docker_obj.return_value.get_size.return_value = size


PIPE_YML = """
name: Bitbucket trigger pipeline
category: Workflow automation
image: bitbucketpipelines/pipelines-trigger-build:1.0.0
description: Trigger a pipeline in a Bitbucket repository.
repository: https://bitbucket.org/atlassian/bitbucket-trigger-pipeline
maintainer:
    name: Atlassian
    website: https://www.atlassian.com/
tags:
    - pipelines
    - pipes
    - build
    - bitbucket
"""

MANIFEST_YML = """
repositoryPath: 'atlassian/bitbucket-trigger-pipeline'
version: '1.0.0'
"""

PIPE_YML_NO_TAGS = """
name: Bitbucket trigger pipeline
category: Workflow automation
image: bitbucketpipelines/pipelines-trigger-build:1.0.0
description: Trigger a pipeline in a Bitbucket repository.
repository: https://bitbucket.org/atlassian/bitbucket-trigger-pipeline
maintainer:
    name: Atlassian
    website: https://www.atlassian.com/
"""

MANIFEST_YML_CUSTOM = """
name: Bitbucket trigger pipeline
description: Trigger a pipeline in a Bitbucket repository.
category: Workflow automation
logo: my-logo.svg
repositoryPath: 'atlassian/bitbucket-trigger-pipeline'
version: '0.1.0'
maintainer:
  name: Atlassian
  website: https://www.atlassian.com/
yml: |
  - pipe: atlassian/bitbucket-trigger-pipeline:0.1.0
    variables:
      APP_PASSWORD: $APP_PASSWORD
      REPO: 'your-awesome-repo'
tags: 
  - my-tag-1
  - my-tag-2
"""


class BuildE2EMockTest(unittest.TestCase):

    def setUp(self):
        self.directory_name = tempfile.mkdtemp()

    @patch('build.DockerHubAPIService')
    @patch('build.BitbucketCloudService')
    def test_extract_yml_definition_is_correct(self, mock_bitbucket, mock_docker):

        self.manifest_path = os.path.join(self.directory_name, 'manifest.yml')
        with open(self.manifest_path, 'w', encoding='utf8') as outfile:
            outfile.write(MANIFEST_YML)

        avatar_url = 'https://bytebucket.org/ravatar'
        created_on = "\"b'Wed, 25 Mar 2020 17:19:34 -0700\\n'\""
        version = '1.0.0'

        configure_mock_bitbucket(mock_bitbucket,
                                 repository_info={'links': {'avatar': {'href': avatar_url}}},
                                 latest_tag=version,
                                 readme_exists=True,
                                 yaml_fragment=YAML_FRAGMENT,
                                 timestamp=created_on,
                                 pipe_yml=PIPE_YML
                                 )
        configure_mock_docker(mock_docker, int(1234232))
        process_result: ProcessResult = build.process(self.manifest_path)
        self.assertTrue(process_result.metadata.valid)

        self.assertDictEqual({
            'name': 'Bitbucket trigger pipeline',
            'description': 'Trigger a pipeline in a Bitbucket repository.',
            'category': 'Workflow automation',
            'repositoryPath': 'atlassian/bitbucket-trigger-pipeline',
            'version': version,
            'maintainer': {'name': 'Atlassian', 'website': 'https://www.atlassian.com/'},
            'logo': avatar_url,
            'yml': YAML_FRAGMENT,
            'tags': ['pipelines', 'pipes', 'build', 'bitbucket'],
            'timestamp': created_on
        },
            process_result.data)

    @patch('build.DockerHubAPIService')
    @patch('build.BitbucketCloudService')
    def test_extract_yml_definition_is_correct_too_large_docker_image(self, mock_bitbucket, mock_docker):

        self.manifest_path = os.path.join(self.directory_name, 'manifest.yml')
        with open(self.manifest_path, 'w', encoding='utf8') as outfile:
            outfile.write(MANIFEST_YML_CUSTOM)

        avatar_url = 'https://bytebucket.org/ravatar'
        created_on = "\"b'Wed, 25 Mar 2020 17:19:34 -0700\\n'\""

        configure_mock_bitbucket(mock_bitbucket,
                                 repository_info={'links': {'avatar': {'href': avatar_url}}},
                                 latest_tag='1.0.0',
                                 readme_exists=True,
                                 yaml_fragment=YAML_FRAGMENT,
                                 timestamp=created_on,
                                 pipe_yml=PIPE_YML
                                 )
        configure_mock_docker(mock_docker, int(99999999999999999))
        process_result: ProcessResult = build.process(self.manifest_path)
        expected_result = ProcessResult(
            metadata=ProcessMetadata(
                file=self.manifest_path,
                valid=False,
                failures=[f'{self.manifest_path} yml field not equal',
                          '1.0.0 version available. Current version is 0.1.0',
                          "Docker image 'bitbucketpipelines/pipelines-trigger-build:1.0.0' is larger than 1GB."],
                errors=[],
                skipped=[]
            ),
            data={
                'name': 'Bitbucket trigger pipeline',
                'description': 'Trigger a pipeline in a Bitbucket repository.',
                'category': 'Workflow automation',
                'logo': 'my-logo.svg',
                'repositoryPath': 'atlassian/bitbucket-trigger-pipeline',
                'version': '0.1.0',
                'maintainer': {'name': 'Atlassian', 'website': 'https://www.atlassian.com/'},
                'yml': "- pipe: atlassian/bitbucket-trigger-pipeline:0.1.0\n  variables:\n    APP_PASSWORD: $APP_PASSWORD\n    REPO: 'your-awesome-repo'\n",
                'tags': ['pipelines', 'pipes', 'build', 'bitbucket'],
                'timestamp': created_on
            }
        )
        self.assertEqual(expected_result, process_result)


MANIFEST_FIXTURE = """
repositoryPath: 'atlassian/ssh-run'
version: '0.2.6'
"""

MANIFEST_CUSTOM_PIPE_OLD_METADATA_FIXTURE = """name: WhiteSource scan
description: Scan and report open sources vulnerabilities and security issues
category: Security
logo: 'https://bytebucket.org/ravatar/%7B0301417f-87a0-4c8d-8f45-969db3a9eb7a%7D?ts=2135667'
repositoryPath: 'WhitesourceSoftware/whitesource-scan'
version: '1.1.0'
vendor:
  name: WhiteSource
  website: https://www.whitesourcesoftware.com/
maintainer:
  name: WhiteSource
  website: https://www.whitesourcesoftware.com/
"""

MANIFEST_NO_CATEGORY_FIXTURE = """name: WhiteSource scan
description: Scan and report open sources vulnerabilities and security issues
logo: 'https://bytebucket.org/ravatar/%7B0301417f-87a0-4c8d-8f45-969db3a9eb7a%7D?ts=2135667'
repositoryPath: 'WhitesourceSoftware/whitesource-scan'
version: '1.1.0'
vendor:
  name: WhiteSource
  website: https://www.whitesourcesoftware.com/
maintainer:
  name: WhiteSource
  website: https://www.whitesourcesoftware.com/
"""

MANIFEST_NON_EXISTENT_CATEGORY = """name: WhiteSource scan
category: Fake
description: Scan and report open sources vulnerabilities and security issues
logo: 'https://bytebucket.org/ravatar/%7B0301417f-87a0-4c8d-8f45-969db3a9eb7a%7D?ts=2135667'
repositoryPath: 'WhitesourceSoftware/whitesource-scan'
version: '1.1.0'
vendor:
  name: WhiteSource
  website: https://www.whitesourcesoftware.com/
maintainer:
  name: WhiteSource
  website: https://www.whitesourcesoftware.com/
"""

MANIFEST_FOR_AZURE_ICON_REMOTE = """
name: Azure Web Apps containers deploy
description: Deploy a container to Azure Web Apps.
category: Deployment
logo: 'https://bytebucket.org/ravatar/%7B96fc02dc-e1f7-41fe-a7c9-f27cef1480b3%7D?ts=2135670'
repositoryPath: 'microsoft/azure-web-apps-containers-deploy'
version: '1.0.2'
vendor:
  name: Microsoft
  website: https://www.microsoft.com/
maintainer:
  name: Microsoft
  website: https://www.microsoft.com/
"""


class BuildE2ERealTest(unittest.TestCase):
    def test_real_check_new_structure(self):

        context = build.extract_information(yaml.safe_load(MANIFEST_FIXTURE), 'pipes/ssh-run.yml')
        failures, skipped = build.validate(context)
        self.assertEqual(len(failures), 0)

    def test_pipe_deprecated_structure(self):

        context = build.extract_information(yaml.safe_load(MANIFEST_CUSTOM_PIPE_OLD_METADATA_FIXTURE),
                                            'pipes/whitesource-scan.yml')
        failures, skipped = build.validate(context)
        self.assertEqual(len(failures), 0)

    def test_pipe_deprecated_structure_icon(self):
        context = build.extract_information(yaml.safe_load(MANIFEST_FOR_AZURE_ICON_REMOTE),
                                            'pipes/azure-web-apps-containers-deploy.yml')
        failures, skipped = build.validate(context)
        self.assertEqual(0, len(failures))

    def test_category_not_found(self):
        context = build.extract_information(yaml.safe_load(MANIFEST_NO_CATEGORY_FIXTURE), 'pipes/whitesource-scan.yml')

        failures, skipped = build.validate(context)
        self.assertEqual(1, len(failures))
        self.assertTrue(str(failures[0]).find("{'category': ['required field']}"))

    def test_category_not_allowed(self):
        context = build.extract_information(yaml.safe_load(MANIFEST_NON_EXISTENT_CATEGORY), 'pipes/whitesource-scan.yml')

        failures, skipped = build.validate(context)
        self.assertEqual(2, len(failures))

        self.assertTrue(str(failures[0]).find("unallowed value Fake"))
        self.assertTrue(str(failures[1]).find("unallowed value Fake"))


if __name__ == '__main__':
    unittest.main()
