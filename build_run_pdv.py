import glob
import json
import sys
import unittest
import requests


class PDV(unittest.TestCase):
    def test_json_match_number_of_files(self):
        list_files = sorted(glob.glob('pipes/*.yml'))
        with open(self.PIPES_FILE) as json_file:
            data = json.load(json_file)

        self.assertEqual(len(data), len(list_files))
        self.assertGreater(len(data), 0)

    def test_file_is_deployed(self):

        with open(self.PIPES_FILE) as json_file:
            data = json.load(json_file)

        r = requests.get(f"https://bitbucket.org/bitbucketpipelines/official-pipes/raw/master/{self.PIPES_FILE}")

        self.assertEqual(r.json(), data)
        self.assertGreater(len(data), 0)


if __name__ == '__main__':
    PDV.PIPES_FILE = sys.argv.pop() if len(sys.argv) > 1 else 'pipes.stag.json'
    unittest.main()
