pyyaml==5.3.1
cerberus==1.3.2
requests==2.23.0
docker==4.2.0
colorlog==4.1.0
GitPython==3.1.1
